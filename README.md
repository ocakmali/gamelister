## GameLister
##### Used Libraries

  - Dagger 2
  - RxJava 2
  - Retrofit
  - Lifecycle
  - Databinding
  - Glide
  
##### Architecture

 - Followed [Android's Architecure Guide](https://developer.android.com/jetpack/docs/guide#connect-viewmodel-repository)
 - ViewState Pattern for ViewModel - View communication more info can be found link below
 
 [ViewState Pattern and Trendyol Android Architecture - Turkish](https://www.youtube.com/watch?v=8__w9K0l0Dw)

### Todos

 - Persistence
 - Tablet support
 - Better design / More info on detail page

License
----
    Copyright 2019 Mehmet Ali Ocak

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
