package com.ocakmali.gamelister.ui.gamedetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ocakmali.gamelister.common.executor.ISchedulerProvider
import com.ocakmali.gamelister.repository.games.GamesRepository
import com.ocakmali.gamelister.ui.common.RxAwareViewModel
import com.ocakmali.gamelister.ui.viewstate.GameDetailViewState
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

class GameDetailViewModel @Inject constructor(
    private val repository: GamesRepository,
    private val scheduler: ISchedulerProvider
) : RxAwareViewModel() {

    private val _gameDetailViewState = MutableLiveData<GameDetailViewState>()

    val gameDetailViewState: LiveData<GameDetailViewState> = _gameDetailViewState

    fun getGameById(id: Long) {
        repository
            .getGameById(id)
            .observeOn(scheduler.ui)
            .subscribe { _gameDetailViewState.value = GameDetailViewState(it) }
            .also { disposables += it }
    }
}