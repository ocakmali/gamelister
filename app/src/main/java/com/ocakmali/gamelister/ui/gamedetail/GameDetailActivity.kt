package com.ocakmali.gamelister.ui.gamedetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.ocakmali.gamelister.R
import com.ocakmali.gamelister.databinding.ActivityGameDetailBinding
import com.ocakmali.gamelister.ui.common.BindingActivity
import com.ocakmali.gamelister.ui.extension.observeNotNull
import com.ocakmali.gamelister.ui.extension.toast
import com.ocakmali.gamelister.ui.viewstate.GameDetailViewState
import javax.inject.Inject

class GameDetailActivity : BindingActivity<ActivityGameDetailBinding>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var gameDetailViewModel: GameDetailViewModel

    override fun layoutRes() = R.layout.activity_game_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initGameDetailViewModel()

        if (savedInstanceState == null) {
            gameDetailViewModel.getGameById(intent.getLongExtra(GAME_ID, 0))
        }
    }

    private fun initGameDetailViewModel() {
        gameDetailViewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(GameDetailViewModel::class.java)

        gameDetailViewModel.gameDetailViewState.observeNotNull(this, ::renderGameDetailViewState)
    }

    private fun renderGameDetailViewState(gameDetailViewState: GameDetailViewState) {
        with(binding) {
            viewState = gameDetailViewState
            executePendingBindings()
        }

        if (gameDetailViewState.getIsError()) {
            toast(gameDetailViewState.getErrorMessage(this))
        }
    }

    companion object {
        fun callingIntent(context: Context, gameId: Long): Intent {
            return Intent(context, GameDetailActivity::class.java).apply {
                putExtra(GAME_ID, gameId)
            }
        }

        private const val GAME_ID = "GAME_ID"
    }
}