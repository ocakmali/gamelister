package com.ocakmali.gamelister.ui.common

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class RxAwareViewModel : ViewModel() {

    protected val disposables = CompositeDisposable()

    override fun onCleared() {
        if (disposables.isDisposed.not()) {
            disposables.dispose()
        }
        super.onCleared()
    }
}
