package com.ocakmali.gamelister.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ocakmali.gamelister.common.executor.ISchedulerProvider
import com.ocakmali.gamelister.common.model.Resource
import com.ocakmali.gamelister.repository.games.GamesRepository
import com.ocakmali.gamelister.repository.model.GameMinimal
import com.ocakmali.gamelister.ui.common.RxAwareViewModel
import com.ocakmali.gamelister.ui.viewstate.GamesViewState
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val gamesRepository: GamesRepository,
    private val scheduler: ISchedulerProvider
) : RxAwareViewModel() {

    private val _gamesViewState = MutableLiveData<GamesViewState>()

    val gamesViewState: LiveData<GamesViewState> get() = _gamesViewState

    private var currentPage = 1

    init {
        getGames(currentPage)
    }

    private fun getGames(page: Int) {
        gamesRepository
            .getGames(PAGE_SIZE, page)
            .observeOn(scheduler.ui)
            .subscribe (::postViewState)
            .also { disposables += it }
    }

    fun postViewState(resource: Resource<List<GameMinimal>>) {
        val currentGameList = _gamesViewState.value?.getGames()

        _gamesViewState.value = GamesViewState(
            Resource(
                status = resource.status,
                data = currentGameList?.plus(resource.data ?: emptyList()),
                error = resource.error
            )
        )
    }

    fun loadMore() {
        if (currentPage < MAX_PAGE_LIMIT) {
            getGames(++currentPage)
        }
    }


    companion object {
        private const val PAGE_SIZE = 30
        private const val MAX_PAGE_LIMIT = 10
    }
}