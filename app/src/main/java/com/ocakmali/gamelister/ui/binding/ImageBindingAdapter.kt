package com.ocakmali.gamelister.ui.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object ImageBindingAdapter {

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun setImage(view: ImageView, imageUrl: String?) {
        if (imageUrl == null) {
            return
        }

        Glide
            .with(view)
            .load(imageUrl)
            .into(view)
    }
}

