package com.ocakmali.gamelister.ui.extension

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

inline fun <T> LiveData<T>.observeNotNull(lifecycleOwner: LifecycleOwner, crossinline observerFn: ((T) -> Unit)) {
    observe(lifecycleOwner, Observer { it?.let { observerFn.invoke(it) } })
}