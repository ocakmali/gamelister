package com.ocakmali.gamelister.ui.viewstate

import com.ocakmali.gamelister.common.model.Resource
import com.ocakmali.gamelister.repository.model.Game

class GameDetailViewState(private val resource: Resource<Game>) : ViewState(resource) {

    private val game get() = resource.data

    fun getImageUrl() = game?.imageUrl

    fun getName() = game?.name

    fun getDescription() = game?.description

    fun getGenresAsText() = game?.genres?.joinToString(separator = ", ") { it.name }
}