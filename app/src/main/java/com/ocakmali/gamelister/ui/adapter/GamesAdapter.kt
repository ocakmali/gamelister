package com.ocakmali.gamelister.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ocakmali.gamelister.R
import com.ocakmali.gamelister.databinding.ItemGameMinimalBinding
import com.ocakmali.gamelister.repository.model.GameMinimal
import com.ocakmali.gamelister.ui.viewstate.GameMinimalItemState

class GamesAdapter(private val onGameClickCallback: (GameMinimal) -> Unit)
    : ListAdapter<GameMinimal, GamesAdapter.GamesViewHolder>(GAME_MINIMAL_DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GamesViewHolder {
        val binding = DataBindingUtil.inflate<ItemGameMinimalBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_game_minimal,
            parent,
            false
        )
        return GamesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GamesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        private val GAME_MINIMAL_DIFF_CALLBACK = object : DiffUtil.ItemCallback<GameMinimal>() {
            override fun areItemsTheSame(oldItem: GameMinimal, newItem: GameMinimal): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: GameMinimal, newItem: GameMinimal): Boolean {
                return oldItem == newItem
            }
        }
    }

    inner class GamesViewHolder(private val binding: ItemGameMinimalBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(gameMinimal: GameMinimal) {
            with(binding) {
                itemState = GameMinimalItemState(gameMinimal)
                executePendingBindings()
            }

            binding.root.setOnClickListener {
                onGameClickCallback.invoke(gameMinimal)
            }
        }
    }
}