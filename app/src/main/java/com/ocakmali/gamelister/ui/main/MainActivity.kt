package com.ocakmali.gamelister.ui.main

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ocakmali.gamelister.R
import com.ocakmali.gamelister.databinding.ActivityMainBinding
import com.ocakmali.gamelister.ui.adapter.GamesAdapter
import com.ocakmali.gamelister.ui.common.BindingActivity
import com.ocakmali.gamelister.ui.common.RecyclerViewScrollListener
import com.ocakmali.gamelister.ui.extension.observeNotNull
import com.ocakmali.gamelister.ui.extension.toast
import com.ocakmali.gamelister.ui.gamedetail.GameDetailActivity
import com.ocakmali.gamelister.ui.viewstate.GamesViewState
import javax.inject.Inject

class MainActivity : BindingActivity<ActivityMainBinding>() {

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var mainViewModel: MainViewModel
    private lateinit var gamesAdapter: GamesAdapter

    override fun layoutRes() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initMainViewModel()
        initGamesRecyclerView()
    }

    private fun initMainViewModel() {
        mainViewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(MainViewModel::class.java)

        mainViewModel.gamesViewState.observeNotNull(this, ::renderGamesState)
    }

    private fun initGamesRecyclerView() {
        gamesAdapter = GamesAdapter {
            startActivity(
                GameDetailActivity.callingIntent(this, it.id)
            )
        }
        val linearLayoutManager = LinearLayoutManager(this)
        val divider = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)

        with(binding.rcvGames) {
            adapter = gamesAdapter
            layoutManager = linearLayoutManager
            addItemDecoration(divider)

            addOnScrollListener(object : RecyclerViewScrollListener(linearLayoutManager) {
                override fun onLoadMore() {
                    mainViewModel.loadMore()
                }

                override fun isLoading() =  mainViewModel.gamesViewState.value?.getIsLoading() ?: false

            })
        }
    }

    private fun renderGamesState(state: GamesViewState) {
        with(binding) {
            viewState = state
            executePendingBindings()
        }

        if (state.getIsError()) {
            toast(state.getErrorMessage(this))
        }

        gamesAdapter.submitList(state.getGames())
    }
}
