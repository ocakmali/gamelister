package com.ocakmali.gamelister.ui.viewstate

import android.content.Context
import com.ocakmali.gamelister.R
import com.ocakmali.gamelister.common.model.Resource
import com.ocakmali.gamelister.common.model.Status
import retrofit2.HttpException
import java.io.IOException

open class ViewState(private val resource: Resource<*>) {

    open fun getIsLoading() = resource.status == Status.LOADING

    open fun getIsError() = resource.status == Status.ERROR

    open fun getErrorMessage(context: Context):String {
        return when (resource.error) {
            is HttpException, is IOException -> context.getString(R.string.please_check_your_internet_connection)
            else -> resource.error?.message ?: context.getString(R.string.unknown_network_error)
        }
    }
}