package com.ocakmali.gamelister.ui.viewstate

import com.ocakmali.gamelister.common.model.Resource
import com.ocakmali.gamelister.repository.model.GameMinimal

class GamesViewState(private val resource: Resource<List<GameMinimal>>) : ViewState(resource) {

    fun getGames() = resource.data ?: listOf()
}