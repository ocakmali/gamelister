package com.ocakmali.gamelister.ui.common

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class RecyclerViewScrollListener constructor(private val linearLayoutManager: LinearLayoutManager) :
    RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (isLoading().not() && canScroll(recyclerView).not()) {
            onLoadMore()
        }
    }

    private fun canScroll(recyclerView: RecyclerView): Boolean {
        return if (linearLayoutManager.orientation == LinearLayoutManager.VERTICAL) {
            recyclerView.canScrollVertically(1)
        } else {
            recyclerView.canScrollHorizontally(1)
        }
    }

    abstract fun onLoadMore()

    abstract fun isLoading(): Boolean
}