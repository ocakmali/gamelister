package com.ocakmali.gamelister.ui.viewstate

import com.ocakmali.gamelister.repository.model.GameMinimal

class GameMinimalItemState(private val gameMinimal: GameMinimal) {

    fun getImageUrl() = gameMinimal.imageUrl

    fun getName() = gameMinimal.name

    fun getRating() = gameMinimal.rating?.toFloat() ?: 0f

    fun getRatingCount() = gameMinimal.ratingCount?.toString() ?: "-"
}