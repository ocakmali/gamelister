package com.ocakmali.gamelister.remote.model

import com.google.gson.annotations.SerializedName

class GamesResponse(
    @SerializedName("results")
    val games: List<GameMinimalRemoteEntity>
)