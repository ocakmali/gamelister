package com.ocakmali.gamelister.remote.model

import com.google.gson.annotations.SerializedName
import com.ocakmali.gamelister.repository.model.Genre

fun GenreRemoteEntity.mapToGenre() = Genre(id = id, name = name)

class GenreRemoteEntity(
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String
)
