package com.ocakmali.gamelister.remote.model

import com.google.gson.annotations.SerializedName
import com.ocakmali.gamelister.repository.model.GameMinimal

fun GameMinimalRemoteEntity.mapToGameMinimal() = GameMinimal(
    id = id, name = name, imageUrl = imageUrl, rating = rating, ratingCount = ratingCount
)

class GameMinimalRemoteEntity(
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String?,
    @SerializedName("background_image")
    val imageUrl: String?,
    @SerializedName("rating")
    val rating: Double?,
    @SerializedName("ratings_count")
    val ratingCount: Long?
)