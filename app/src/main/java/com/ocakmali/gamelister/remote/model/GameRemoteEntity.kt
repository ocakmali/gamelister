package com.ocakmali.gamelister.remote.model

import com.google.gson.annotations.SerializedName
import com.ocakmali.gamelister.repository.model.Game

fun GameRemoteEntity.mapToGame() = Game(
    id = id,
    imageUrl = imageUrl,
    name = name,
    description = description,
    genres = genres?.map { it.mapToGenre() }
)

class GameRemoteEntity(
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String,
    @SerializedName("description_raw")
    val description: String?,
    @SerializedName("background_image")
    val imageUrl: String?,
    @SerializedName("genres")
    val genres: List<GenreRemoteEntity>?
)