package com.ocakmali.gamelister.remote.games

import com.ocakmali.gamelister.remote.api.GameListerService
import com.ocakmali.gamelister.remote.model.GameMinimalRemoteEntity
import com.ocakmali.gamelister.remote.model.GameRemoteEntity
import com.ocakmali.gamelister.repository.games.IGamesRemoteDataSource
import io.reactivex.Observable
import javax.inject.Inject

class GamesRemoteDataSource @Inject constructor(
    private val service: GameListerService
) : IGamesRemoteDataSource {

    override fun getGames(pageSize: Int, page: Int): Observable<List<GameMinimalRemoteEntity>> {
        return service
            .getGames(pageSize, page)
            .map { it.games }
    }

    override fun getGameById(id: Long): Observable<GameRemoteEntity> {
        return service
            .getGameById(id)
    }
}