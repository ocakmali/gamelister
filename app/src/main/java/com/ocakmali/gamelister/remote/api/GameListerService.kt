package com.ocakmali.gamelister.remote.api

import com.ocakmali.gamelister.remote.model.GameRemoteEntity
import com.ocakmali.gamelister.remote.model.GamesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GameListerService {

    @GET("games")
    fun getGames(
        @Query("page_size") pageSize: Int,
        @Query("page") page: Int
    ): Observable<GamesResponse>

    @GET("games/{id}")
    fun getGameById(
        @Path("id") gameId: Long
    ): Observable<GameRemoteEntity>
}
