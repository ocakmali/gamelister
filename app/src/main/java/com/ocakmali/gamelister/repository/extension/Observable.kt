package com.ocakmali.gamelister.repository.extension

import com.ocakmali.gamelister.common.model.Resource
import io.reactivex.Observable

fun <R> Observable<Resource<R>>.applyLoading(): Observable<Resource<R>> {
    return Observable.just(Resource.loading<R>(null)).concatWith(this)
}

fun <R> Observable<Resource<R>>.onError(): Observable<Resource<R>> {
    return onErrorReturn { Resource.error(it) }
}