package com.ocakmali.gamelister.repository.model

data class Genre(
    val id: Long,
    val name: String
)
