package com.ocakmali.gamelister.repository.games

import com.ocakmali.gamelister.remote.model.GameMinimalRemoteEntity
import com.ocakmali.gamelister.remote.model.GameRemoteEntity
import io.reactivex.Observable

interface IGamesRemoteDataSource {

    fun getGames(pageSize: Int, page: Int): Observable<List<GameMinimalRemoteEntity>>

    fun getGameById(id: Long): Observable<GameRemoteEntity>
}