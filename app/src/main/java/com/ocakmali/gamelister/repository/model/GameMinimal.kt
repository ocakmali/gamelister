package com.ocakmali.gamelister.repository.model

data class GameMinimal(
    val id: Long,
    val name: String?,
    val imageUrl: String?,
    val rating: Double?,
    val ratingCount: Long?
)