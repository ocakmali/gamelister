package com.ocakmali.gamelister.repository.model

data class Game(
    val id: Long,
    val name: String,
    val description: String?,
    val imageUrl: String?,
    val genres: List<Genre>?
)
