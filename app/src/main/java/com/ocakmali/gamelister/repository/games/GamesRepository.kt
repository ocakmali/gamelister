package com.ocakmali.gamelister.repository.games

import com.ocakmali.gamelister.common.executor.ISchedulerProvider
import com.ocakmali.gamelister.common.model.Resource
import com.ocakmali.gamelister.remote.model.mapToGame
import com.ocakmali.gamelister.remote.model.mapToGameMinimal
import com.ocakmali.gamelister.repository.extension.applyLoading
import com.ocakmali.gamelister.repository.extension.onError
import com.ocakmali.gamelister.repository.model.Game
import com.ocakmali.gamelister.repository.model.GameMinimal
import io.reactivex.Observable
import javax.inject.Inject

class GamesRepository @Inject constructor(
    private val remoteDataSource: IGamesRemoteDataSource,
    private val scheduler: ISchedulerProvider
) {

    fun getGames(pageSize: Int, page: Int): Observable<Resource<List<GameMinimal>>> {
        return remoteDataSource
            .getGames(pageSize, page)
            .map { gameMinimalList ->
                Resource.success(
                    gameMinimalList.map { it.mapToGameMinimal() }
                )
            }
            .onError()
            .applyLoading()
            .subscribeOn(scheduler.io)
    }

    fun getGameById(id: Long): Observable<Resource<Game>> {
        return remoteDataSource
            .getGameById(id)
            .map { Resource.success(it.mapToGame()) }
            .onError()
            .applyLoading()
            .subscribeOn(scheduler.io)
    }
}