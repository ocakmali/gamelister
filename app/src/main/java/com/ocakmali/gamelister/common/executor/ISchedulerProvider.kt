package com.ocakmali.gamelister.common.executor

import io.reactivex.Scheduler

interface ISchedulerProvider {

    val ui: Scheduler

    val io: Scheduler

    val computation: Scheduler
}