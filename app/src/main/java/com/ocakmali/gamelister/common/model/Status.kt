package com.ocakmali.gamelister.common.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}