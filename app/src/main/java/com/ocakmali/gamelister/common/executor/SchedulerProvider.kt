package com.ocakmali.gamelister.common.executor

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class SchedulerProvider @Inject constructor() : ISchedulerProvider {

    override val ui get() = AndroidSchedulers.mainThread()

    override val io get() = Schedulers.io()

    override val computation get() = Schedulers.computation()
}