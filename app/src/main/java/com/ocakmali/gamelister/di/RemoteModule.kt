package com.ocakmali.gamelister.di

import com.ocakmali.gamelister.remote.api.GameListerService
import com.ocakmali.gamelister.remote.games.GamesRemoteDataSource
import com.ocakmali.gamelister.repository.games.IGamesRemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
abstract class RemoteModule {

    @Module
    companion object {

        @Singleton
        @Provides
        @JvmStatic
        fun provideGameListerService(): GameListerService {
            return Retrofit.Builder()
                .baseUrl("https://api.rawg.io/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(GameListerService::class.java)
        }
    }

    @Binds
    abstract fun bindGameRemoteDataSource(dataSource: GamesRemoteDataSource): IGamesRemoteDataSource
}