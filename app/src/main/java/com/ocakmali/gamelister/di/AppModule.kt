package com.ocakmali.gamelister.di

import com.ocakmali.gamelister.common.executor.ISchedulerProvider
import com.ocakmali.gamelister.common.executor.SchedulerProvider
import dagger.Binds
import dagger.Module

@Module
interface AppModule {

    @Binds
    fun bindSchedulerProvider(shedulerProvider: SchedulerProvider): ISchedulerProvider
}