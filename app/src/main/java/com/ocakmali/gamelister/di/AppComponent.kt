package com.ocakmali.gamelister.di

import com.ocakmali.gamelister.ui.GameListerApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuildersModule::class,
        RemoteModule::class,
        ViewModelModule::class,
        AppModule::class
    ]
)
interface AppComponent : AndroidInjector<GameListerApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: GameListerApp): Builder

        fun build(): AppComponent
    }
}