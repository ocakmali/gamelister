package com.ocakmali.gamelister.di

import com.ocakmali.gamelister.ui.gamedetail.GameDetailActivity
import com.ocakmali.gamelister.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBuildersModule {

    @ContributesAndroidInjector
    fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    fun contributeGameDetailActivity(): GameDetailActivity
}